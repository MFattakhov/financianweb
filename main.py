from datetime import date
from flask import Flask, url_for, render_template, request, redirect, session
from forms import LoginForm, RegisterForm, ButtonForm, NewForm, DateForm
from models import UsersModel, DB, SpendingsModel

app = Flask(__name__)
app.config['SECRET_KEY'] = 'my_secret_key'
db = DB('db.sqlite')
model = SpendingsModel(db.get_connection())
model.init_table()
user_model = UsersModel(db.get_connection())
user_model.init_table()


@app.route('/')
@app.route('/login', methods=['POST', 'GET'])
def login():
    login_form = LoginForm()
    register_form = RegisterForm()

    if login_form.validate_on_submit():
        user_name = login_form.username.data
        password = login_form.password.data
        exists = user_model.exists(user_name, password)
        if (exists[0]):
            session['username'] = user_name
            session['user_id'] = exists[1]
            return redirect(url_for('main'))
        else:
            return render_template('login.html', pass_incorrect=False, error=(True, 'Invalid data'),
                                   login_form=login_form, register_form=register_form)

    if register_form.validate_on_submit():
        user_name = register_form.username.data
        password_first = register_form.password_first.data
        password_second = register_form.password_second.data
        if password_first == password_second:
            user_model.insert(user_name, password_first)
            exists = user_model.exists(user_name, password_first)
            session['username'] = user_name
            session['user_id'] = exists[1]
            return redirect(url_for('main'))
        else:
            return render_template('login.html', pass_incorrect=True, error=None,
                                   login_form=login_form, register_form=register_form)

    return render_template('login.html', pass_incorrect=False, error=None,
                           login_form=login_form, register_form=register_form)


@app.route('/main')
def main_without_date():
    return redirect(f'/main/{str(date.today())}')


@app.route('/main/<date_came>', methods=['POST', 'GET'])
def main(date_came):
    if 'username' not in session:
        return redirect(url_for('login'))

    session['date'] = date_came

    date_form = DateForm()
    if date_form.validate_on_submit():
        date = date_form.date.data
        date = date.split('/')
        try:
            if 1 <= int(date[1]) <= 31 and 1 <= int(date[0]) <= 12 and 1910 <= int(date[2]) <= 2019:
                date = '-'.join([date[2], date[0], date[1]])
                return redirect(f'/main/{date}')
            else:
                return redirect(f'/main/{date_came}')
        except:
            return redirect(f'/main/{date_came}')

    spendings = SpendingsModel(db.get_connection()).get_all(user_id=session['user_id'],
                                                            date=date_came)
    return render_template('main.html', username=session['username'], spendings=spendings,
                           date_form=date_form, date_came='/'.join(date_came.split('-')))


@app.route('/new', methods=['POST', 'GET'])
def new():
    if 'username' not in session:
        return redirect(url_for('login'))

    form = NewForm()
    if form.validate_on_submit():
        value = form.value.data
        commend = form.commend.data
        model = SpendingsModel(db.get_connection())
        model.insert(value, commend, session['user_id'], session['date'])
        return redirect(f'/main/{session["date"]}')

    return render_template('new.html', form=form)


@app.route('/logout')
def logout():
    session.pop('username')
    session.pop('user_id')
    return redirect(url_for('login'))


@app.errorhandler(404)
def error404(error):
    return render_template('error.html', error_text='404 page not found.')


@app.errorhandler(405)
def error405(error):
    return render_template('error.html', error_text='405 unexpected method.')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, use_reloader=True)

# is doesn't matter, bro
