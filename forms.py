from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, TextAreaField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class LoginForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    submit = SubmitField('Войти')


class RegisterForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    password_first = PasswordField('Пароль', validators=[DataRequired()])
    password_second = PasswordField('Пароль', validators=[DataRequired()])
    submit = SubmitField('Зарегестрироваться')


class ButtonForm(FlaskForm):
    submit = SubmitField('')


class NewForm(FlaskForm):
    value = IntegerField(validators=[DataRequired(), NumberRange(min=1)])
    commend = TextAreaField('')
    submit = SubmitField('')


class DateForm(FlaskForm):
    date = StringField(validators=[DataRequired()])
    submit = SubmitField()
