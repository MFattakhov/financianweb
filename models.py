import sqlite3


class DB:
    def __init__(self, name):
        conn = sqlite3.connect((name), check_same_thread=False)
        self.conn = conn

    def get_connection(self):
        return self.conn

    def __del__(self):
        self.conn.close()


class UsersModel:
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS users
                        (id INTEGER PRIMARY KEY AUTOINCREMENT,
                         user_name VARCHAR(50),
                         password_hash VARCHAR(128))''')
        cursor.close()
        self.connection.commit()

    def get(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users WHERE id = ?", (str(user_id),))
        row = cursor.fetchone()
        return row

    def get_all(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users")
        rows = cursor.fetchall()
        return rows

    def exists(self, user_name, password_hash):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users WHERE user_name = ? AND password_hash = ?",
                       (user_name, password_hash))
        row = cursor.fetchone()
        return (True, row[0]) if row else (False,)

    def insert(self, user_name, password_hash):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO users
                        (user_name, password_hash)
                        VALUES (?,?)''', (user_name, password_hash))
        cursor.close()
        self.connection.commit()


class SpendingsModel:
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS spendings
                            (id INTEGER PRIMARY KEY AUTOINCREMENT,
                            value VARCHAR(16),
                            commend VARCHAR(256),
                            user_id INTEGER,
                            date TEXT
                            )''')
        cursor.close()
        self.connection.commit()

    def insert(self, value, commend, user_id, date):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO spendings
                        (value, commend, user_id, date)
                        VALUES (?,?,?,?)''', (value, commend, str(user_id), date))
        cursor.close()
        self.connection.commit()

    def get_all(self, user_id=None, date=None):
        cursor = self.connection.cursor()
        if user_id:
            cursor.execute("SELECT * FROM spendings WHERE user_id = ? AND date = ?",
                           (str(user_id), date))
        else:
            cursor.execute("SELECT * FROM spendings WHERE date = ?", (date,))
        rows = cursor.fetchall()
        return rows

    def delete(self, spendings_id):
        cursor = self.connection.cursor()
        cursor.execute('''DELETE FROM spendings WHERE id = ?''', (str(spendings_id),))
        cursor.close()
        self.connection.commit()
