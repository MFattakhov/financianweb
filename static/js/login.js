$(document).ready(function () {
    $(function () {
        $(".register_form").hide();
    });

    $(".log_in_btn").click(function () {
        $("#sign_up").addClass("disabled");
        $("#log_in").removeClass("disabled");
        $(".login_form").show();
        $(".register_form").hide();
    });

    $(".sign_in_btn").click(function () {
        $("#log_in").addClass("disabled");
        $("#sign_up").removeClass("disabled");
        $(".login_form").hide();
        $(".register_form").show();
    });
});